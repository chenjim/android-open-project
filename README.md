## Android 开源项目分类汇总

>未收录部分可以参见 [Github Trinea android-open-project](https://github.com/Trinea/android-open-project)


### 个性化控件(View)

- HeartLayout  
  心形❤点赞动画  
  项目地址 https://github.com/tyrantgit/HeartLayout  
  效果图  
<img src="https://images.gitee.com/uploads/images/2020/0508/111658_44d3c2a1_342580.gif" height = "300" />

- ImageCoverFlow  
  图片轮播  
  项目地址 https://github.com/lishide/ImgCoverFlow  
  项目地址 https://github.com/dolphinwang/ImageCoverFlow  
  效果图  
<img src="https://images.gitee.com/uploads/images/2020/0508/112030_417217c9_342580.jpeg" height = "300" />

- MultiViewPager  
  多个ViewPager  
  项目地址 https://github.com/Pixplicity/MultiViewPager  
  效果图  
<img src="http://i.imgur.com/0yGMSyE.gif" width = "300" />

- CircleProgress  
  圆形进度条  
  项目地址 https://github.com/lzyzsd/CircleProgress  
  效果图  
<img src="https://images.gitee.com/uploads/images/2020/0508/133324_b7abb095_342580.gif" height = "300" />


### 工具篇

- MultiThreadDownloader  
  多线程下载  
  项目地址  https://github.com/AigeStudio/MultiThreadDownloader  

- libGDX  
  基于OpenGL（ES）的跨平台的游戏开发框架支持Windows, Linux, Mac OS X, Android, your WebGL enabled browser and iOS.  
  项目地址 https://github.com/libgdx/libgdx/

- PermissionsDispatcher  
  权限动态申请  
  项目地址 https://github.com/permissions-dispatcher/PermissionsDispatcher  

### 优秀项目

- WelikeAndroid  
  一款引入即用的便捷开发框架,包含五个大模块:异常安全隔离模块,Http模块,Bitmap模块,Database模块,ui操纵模块  
  项目地址 https://gitee.com/lody/WelikeAndroid  

- playcamera  
 《Android 3D游戏开发技术宝典--OpenGL ES 2.0》中的例子  
  项目地址 https://github.com/muojie/playcamera  

- 反编译  
  jadx 项目地址 https://github.com/skylot/jadx  
  JD-GUI 项目地址 https://github.com/java-decompiler/jd-gui  
  dex2jar 项目地址 https://github.com/pxb1988/dex2jar  


  


  




